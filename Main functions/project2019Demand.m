%Time-Series project
%Number of team=19
%Papas Ioannis
%9218

%------------Demand Time-Series analysis------------

%Variables
hour=mod(19,24)+1;
placeN=(mod(19,7)+1)+4;
trendMA=14;
demandM=readtable('Demand.xls');
demandV=table2array(demandM((hour:24:24*365), placeN));

%----------������ �-----------

%(1)
figure(1)
clf
plot(demandV,'.-')
hold on
plot(movingaveragesmooth2(demandV, trendMA),'.-')
title('Demand')
legend('Data of demand', sprintf('MA(%d) for trend', trendMA))

demandVSmoothed=demandV-movingaveragesmooth2(demandV, trendMA);

figure(2)
clf
plot(demandVSmoothed,'.-')
title('Detrended Demand')
legend('Demand without trend')

demandVSmoothed=demandVSmoothed-seasonalcomponents(demandVSmoothed, 7);

figure(3)
clf
plot(demandVSmoothed,'.-')
title('Deseasoned and detrended series of demand')

%(2)
figure(4)
clf
corr=autocorrelation(demandVSmoothed, 10, 'Time Series of Demand', 'd');

[h,pV,~,~]=portmanteauLB(corr(:,2), length(demandV), 0.05, 'Portmanteau check');
if(sum(h)==length(corr))
    fprintf("The time series is not white noise\n");
end

%(3)
p=0;
q=0;
partCorr=acf2pacf(corr(2:length(corr(:,2)),2),1);
corrCI=[-2/sqrt(length(demandVSmoothed)) 2/sqrt(length(demandVSmoothed))];
for iC=1:length(partCorr)
    if(partCorr(iC)>corrCI(2) || partCorr(iC)<corrCI(1))
        p=iC;
    end
end
for iP=2:length(corr(:,2))
    if(corr(iP,2)>corrCI(2) || corr(iP,2)<corrCI(1))
        q=iP-1;
    end
end

[phiV,thetaV,SDz,aicS,fpeS,armamodel]=fitARMA(demandVSmoothed,p,q,2);

fprintf("The AIC stat for the demand time series is %f\n", aicS);

%(4)
prediction1=predictARMAmultistep(demandVSmoothed,length(demandV),5,0,1,'');
fprintf("The prediction for 1 day after is %f\n", prediction1+demandV(length(demandV)));

timeDivision=floor([0.3 0.35 0.4 0.45 0.5]*length(demandV));
nrmseV1=zeros(length(timeDivision),1);
for iV=1:length(timeDivision)
    [nrmseV1(iV),~,~,~] = predictARMAnrmse(demandVSmoothed,5,0,1,timeDivision(iV),'');
end

%----------������ �-----------

%(1)
zV=zeros(length(demandVSmoothed),1);
demMean=mean(demandVSmoothed);
for i=(p+1):length(zV)
    zV(i)=demandVSmoothed(i)-phiV(1)-phiV(2)*demandVSmoothed(i-1)-phiV(3)*demandVSmoothed(i-2)-phiV(4)*demandVSmoothed(i-3)+thetaV(1)*zV(i-1)+thetaV(2)*zV(i-2);
end
zV(1:p)=[];

%(2)
iidM=zeros(21,length(zV));
iidM(1,:)=zV;
for iM=2:length(iidM(:,1))
    u=randperm(length(zV));
    iidM(iM,:)=zV(u);
end

%(3)

corrV=zeros(length(iidM(:,1)),1);
partCorrV=zeros(length(iidM(:,1)),1);

for i=1:length(iidM(:,1))
    tmp=autocorrelation(iidM(i,:),5);
    corrV(i)=tmp(4,2);  %�� �������� ��� ������������� 4�� ���������
    tmp2=acf2pacf(tmp(2:length(tmp),2));
    partCorrV(i)=tmp2(2); %������ ������������� ��� 4�� ���������
end

maxLyapV=zeros(length(iidM(:,1)),1);
mutInfV=zeros(length(iidM(:,1)),1);
corrDimV=zeros(length(iidM(:,1)),1);

for i=1:length(iidM(:,1))
    tmp=mutualinformation(iidM(i,:),1);
    mutInfV(i)=tmp(2,2);
    tmp2=maxlyapunov(iidM(i,:)',5,2,2);
    maxLyapV(i)=tmp2(1);
end

for i=1:length(iidM(:,1))
    [~,~,~,~,nuM] = correlationdimension(iidM(i,:)', 5, 2);
    corrDimV(i)=nuM(2,4);
end

%(4)
figure(6)
clf
histogram(corrV(2:length(corrV)),'NumBins', 5)
hold on
line([corrV(1) corrV(1)], [0 15], 'LineStyle','--', 'Color', 'red')
title('Histogram of the correlations of 4th delay')
xlabel('Value of correlation')
legend("Correlations of the 20 iid's", "Correlation of the initial time-series")

figure(7)
clf
histogram(partCorrV(2:length(partCorrV)), 'NumBins', 5)
hold on
line([partCorrV(1) partCorrV(1)], [0 15], 'LineStyle','--', 'Color', 'red')
title('Histogram of the partial correlations of 4th delay')
xlabel('Value of partial correlation')
legend("Partial correlations of the 20 iid's", "Partial correlation of the initial time-series")

figure(8)
clf
histogram(mutInfV(2:length(mutInfV)), 'NumBins', 5)
hold on
line([mutInfV(1) mutInfV(1)], [0 15], 'LineStyle','--', 'Color', 'red')
title('Histogram of the mutual information of 1st delay')
xlabel('Value of mutual information')
legend("Mutual information of the 20 iid's", "Mutual information of the initial time-series")

figure(9)
clf
histogram(maxLyapV(2:length(maxLyapV)), 'NumBins', 5)
hold on
line([maxLyapV(1) maxLyapV(1)], [0 15], 'LineStyle','--', 'Color', 'red')
title('Histogram of the maximum Lyapunov exponent of 5th delay and embedding dimension 2')
xlabel('Value of max Lyapunov exponent')
legend("Maximum Lyapunov exponent of the 20 iid's", "Maximum Lyapunov exponent of the initial time-series")

figure(10)
clf
histogram(corrDimV(2:length(corrDimV)), 'NumBins', 5)
hold on
line([corrDimV(1) corrDimV(1)], [0 15], 'LineStyle','--', 'Color', 'red')
title('Histogram of the correlation dimension of 5th delay and embedding dimension 2')
xlabel('Value of correlation dimension')
legend("Correlation dimension of the 20 iid's", "Correlation dimension of the initial time-series")


