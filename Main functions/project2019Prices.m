%Time-Series project
%Number of team=19
%Papas Ioannis
%9218

%------------Price Time-Series analysis------------


%Variables
hour=mod(19,24)+1;
placeN=(mod(19,7)+1)+4;
pricesM=readtable('ElectricPowerItaly.xls');
pricesV=table2array(pricesM((hour:24:24*365), placeN));
t=length(pricesV);

%(1)
figure(1)
clf
plot(pricesV,'.-')
title('Price')
legend('Data of price')

pricesVSmoothed=zeros(t,1);
pricesVSmoothed(1)=pricesV(1);
pricesVSmoothed(2:t)=pricesV(2:t)-pricesV(1:t-1);

figure(2)
clf
plot(pricesVSmoothed(2:t),'.-')
title('Detrended Price')
legend('Price without trend')

pricesVSmoothed=pricesVSmoothed-seasonalcomponents(pricesVSmoothed, 7);

figure(3)
clf
plot(pricesVSmoothed(2:t),'.-')
title('Deseasoned and detrended series of price')

%(2)
figure(4)
clf
corr=autocorrelation(pricesVSmoothed, 10, 'Time Series of Prices', 'd');

[h,pV,~,~]=portmanteauLB(corr(:,2), length(pricesV), 0.05, 'Portmanteau check');
if(sum(h)==length(corr))
    fprintf("The time series is not white noise\n");
end

%(3)
p=0;
q=0;
partCorr=acf2pacf(corr(2:length(corr(:,2)),2),1);
corrCI=[-2/sqrt(length(pricesVSmoothed)) 2/sqrt(length(pricesVSmoothed))];
for iC=1:length(partCorr)
    if(partCorr(iC)>corrCI(2) || partCorr(iC)<corrCI(1))
        p=iC;
    end
end
for iP=2:length(corr(:,2))
    if(corr(iP,2)>corrCI(2) || corr(iP,2)<corrCI(1))
        q=iP-1;
    end
end

[phiV,thetaV,SDz,aicS,fpeS,armamodel]=fitARMA(pricesVSmoothed,p,q,2);

fprintf("The AIC stat for the demand time series is %f", aicS);

%(4)
prediction1=predictARMAmultistep(pricesVSmoothed,length(pricesV),5,0,1,'');
fprintf("The prediction for 1 day after is %f\n", prediction1+pricesV(length(pricesV)));

timeDivision=floor([0.3 0.35 0.4 0.45 0.5]*length(pricesV));
nrmseV=zeros(length(timeDivision),1);
for iV=1:length(timeDivision)
    [nrmseV(iV),~,~,~] = predictARMAnrmse(pricesVSmoothed,5,0,1,timeDivision(iV),'');
end

%----------������ �-----------

%(1)
zV=zeros(length(demandVSmoothed),1);
demMean=mean(demandVSmoothed);
for i=(p+1):length(zV)
    zV(i)=demandVSmoothed(i)-phiV(1)-phiV(2)*demandVSmoothed(i-1)-phiV(3)*demandVSmoothed(i-2)-phiV(4)*demandVSmoothed(i-3)-phiV(5)*demandVSmoothed(i-4)-phiV(6)*demandVSmoothed(i-5)+thetaV(1)*zV(i-1);
end
zV(1:p)=[];

%(2)
iidM=zeros(21,length(zV));
iidM(1,:)=zV;
for iM=2:length(iidM(:,1))
    u=randperm(length(zV));
    iidM(iM,:)=zV(u);
end

%(3)corrV=zeros(length(iidM(:,1)),1);
partCorrV=zeros(length(iidM(:,1)),1);

for i=1:length(iidM(:,1))
    tmp=autocorrelation(iidM(i,:),5);
    corrV(i)=tmp(4,2);  %�� �������� ��� ������������� 4�� ���������
    tmp2=acf2pacf(tmp(2:length(tmp),2));
    partCorrV(i)=tmp2(2); %������ ������������� ��� 4�� ���������
end

maxLyapV=zeros(length(iidM(:,1)),1);
mutInfV=zeros(length(iidM(:,1)),1);
corrDimV=zeros(length(iidM(:,1)),1);

for i=1:length(iidM(:,1))
    tmp=mutualinformation(iidM(i,:),1);
    mutInfV(i)=tmp(2,2);
    tmp2=maxlyapunov(iidM(i,:)',5,2,2);
    maxLyapV(i)=tmp2(1);
end

for i=1:length(iidM(:,1))
    [~,~,~,~,nuM] = correlationdimension(iidM(i,:)', 5, 2);
    corrDimV(i)=nuM(2,4);
end

%(4)
figure(6)
clf
histogram(corrV(2:length(corrV)),'NumBins', 5)
hold on
line([corrV(1) corrV(1)], [0 15], 'LineStyle','--', 'Color', 'red')
title('Histogram of the correlations of 4th delay')
xlabel('Value of correlation')
legend("Correlations of the 20 iid's", "Correlation of the initial time-series")

figure(7)
clf
histogram(partCorrV(2:length(partCorrV)), 'NumBins', 5)
hold on
line([partCorrV(1) partCorrV(1)], [0 15], 'LineStyle','--', 'Color', 'red')
title('Histogram of the partial correlations of 4th delay')
xlabel('Value of partial correlation')
legend("Partial correlations of the 20 iid's", "Partial correlation of the initial time-series")

figure(8)
clf
histogram(mutInfV(2:length(mutInfV)), 'NumBins', 5)
hold on
line([mutInfV(1) mutInfV(1)], [0 15], 'LineStyle','--', 'Color', 'red')
title('Histogram of the mutual information of 4th delay')
xlabel('Value of mutual information')
legend("Mutual information of the 20 iid's", "Mutual information of the initial time-series")

figure(9)
clf
histogram(maxLyapV(2:length(maxLyapV)), 'NumBins', 5)
hold on
line([maxLyapV(1) maxLyapV(1)], [0 15], 'LineStyle','--', 'Color', 'red')
title('Histogram of the maximum Lyapunov exponent of 5th delay and embedding dimension 2')
xlabel('Value of max Lyapunov exponent')
legend("Maximum Lyapunov exponent of the 20 iid's", "Maximum Lyapunov exponent of the initial time-series")

figure(10)
clf
histogram(corrDimV(2:length(corrDimV)), 'NumBins', 5)
hold on
line([corrDimV(1) corrDimV(1)], [0 15], 'LineStyle','--', 'Color', 'red')
title('Histogram of the correlation dimension of 5th delay and embedding dimension 2')
xlabel('Value of correlation dimension')
legend("Correlation dimension of the 20 iid's", "Correlation dimension of the initial time-series")
